$(document).ready(function () {
    $("#start").on("click", function () {
        $("#start").attr("disabled", true);        
        let mysec = 0
        let mymin = 0
        let myhour = 0
        const myInterval = setInterval(function () {
            if (mysec < 59) {
                mysec++
            }
            else if (mymin < 59) {
                mysec = 0
                mymin++
            } else {
                console.log("else");
                mymin = 0
                mysec = 0
                myhour++
            }

            $("#timer").html((myhour > 9 ? myhour : "0" + myhour) + " : " + (mymin > 9 ? mymin : "0" + mymin) + " : " + (mysec > 9 ? mysec : "0" + mysec))
        }, 1000)
        $("#reset").click(function () {
            clearInterval(myInterval)
            $("#start").removeAttr("disabled"); 
            $("#timer").html("00 : 00 : 00")
        })

    });
})

